package com.aierjun.moveview.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.aierjun.moveview.R;

public class MainActivity extends AppCompatActivity {
    private Button moveImage;
    private Button moveVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
        initView();
    }

    private void findView() {
        moveImage= (Button) findViewById(R.id.move_image);
        moveVideo= (Button) findViewById(R.id.move_video);
    }

    private void initView() {
        moveImage.setOnClickListener(ImageOnClickListener);
        moveVideo.setOnClickListener(VideoOnClickListener);
    }

    private View.OnClickListener ImageOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(MainActivity.this,MoveImageViewActivity.class));
        }
    };
    private View.OnClickListener VideoOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(MainActivity.this,MoveVideoViewActivity.class));
        }
    };
}
