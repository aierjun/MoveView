package com.aierjun.moveview.activity;

import android.app.Activity;
import android.os.Bundle;

import com.aierjun.moveview.R;

/**
 * Created by aierJun on 2016/12/21.
 */
public class MoveVideoViewActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoview);
    }

    @Override
    protected void onDestroy() {
        //VideoView.finis();  注意点
        super.onDestroy();
    }
}
