package com.aierjun.moveview.activity;

import android.app.Activity;
import android.os.Bundle;

import com.aierjun.moveview.R;

/**
 * Created by aierJun on 2016/12/21.
 */
public class MoveImageViewActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageview);
    }
}
